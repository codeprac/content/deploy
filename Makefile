cluster_name := codeprac-content
namespace := content
env ?= sample

init:
	kind create cluster --config ./kind.yaml --name $(cluster_name)
teardown:
	kind delete cluster --name $(cluster_name)

load-image: .check-context
	kind load docker-image --name $(cluster_name) registry.gitlab.com/codeprac/cms:latest

install: .check-context .check-name
	@$(MAKE) .helm \
		command="upgrade --install --create-namespace"
debug: .check-context .check-name
	@$(MAKE) .helm \
		command="template --dry-run --debug"
deploy: .check-name .deploy-timeout
	@$(MAKE) .helm \
		command="upgrade --install --create-namespace"
debug-deploy: .check-name
	@$(MAKE) .helm \
		command="template --dry-run --debug"
.helm:
	helm $(command) \
		--namespace $(namespace) \
		--values ./charts/$(name)/values.yaml \
		--values ./charts/$(name)/values/$(env).yaml \
		--values secrets://charts/$(name)/secrets/$(env).yaml \
		"content-$(name)" ./charts/$(name)

.deploy-timeout:
	@echo "============================"
	@echo "Deploying '$(name)' to:"
	@echo "  Cluster:     $$(kubectl config current-context)"
	@echo "  Namespace:   $(namespace)"
	@echo "  Environment: $(env)"
	@echo "============================"
	@printf -- "> Confirm? (only 'yes' will be accepted) "
	@read confirmation && if [ "$$confirmation" != "yes" ]; then exit 1; fi;
.check-context:
	@if ! kubectl config current-context | grep kind-$(cluster_name) 1>/dev/null; then \
		echo "You are not in the $(cluster_name) context, use 'kubectl config use-context kind-$(cluster_name)' fix this"; \
		exit 1; \
	fi
.check-name:
ifndef name
	@echo "The required 'name' was not defined (eg. name=content)"
	@exit 1
endif
