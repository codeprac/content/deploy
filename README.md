# Content Deployment

This repository contains manifests for deploying the `content` service.

# Pre-requisites

1. **Helm 3** ([installation instructions](https://helm.sh/docs/intro/install/)) - used for Kubernetes resource management
2. **SOPS** ([installation instructions](https://github.com/mozilla/sops)) - used for secrets management
3. **Helm Secrets** ([installation instructions](https://github.com/jkroepke/helm-secrets#using-helm-plugin-manager)) - used for just-in-time decryption of SOPS-encrypted secrets

# Overview

## Testing this locally

1. To test this locally, bring up the Kubernetes-in-Docker (`kind`) cluster using `make init` (teardown using `make teardown`)

## Deploying charts

1. Chart names are the directory names listed in `./charts`
2. Run `make install name=${CHART_NAME}` to install the selected chart where `${CHART_NAME}` is the directory name
3. Enter in your MFA for decrypting the secrets

## Managing secrets

1. Set your `AWS_PROFILE` to the correct profile as listed in your `~/.aws/credentials` or `~/.aws/profiles`
2. Run `sops edit ./path/to/secret-file.yaml`
